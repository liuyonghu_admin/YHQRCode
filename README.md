#YHQRCode
二维码快速扫描工具

### 项目简介：
集成系统硬件权限获取、二维码扫描功能。

### 项目主要应用技术点
调用系统相机设备 _AVCaptureDevice_ 指定输出与输入:
```
AVCaptureDevice *device = [AVCaptureDevice  defaultDeviceWithMediaType:AVMediaTypeVideo];
NSError *error;
AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc]init];
[output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
```
设置输出元数据格式:
```
output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
```
配置显示图层:
```
_previewlayer = [AVCaptureVideoPreviewLayer layerWithSession:_captureSession];
    
    //   显示方式  --  填充
_previewlayer.videoGravity  = AVLayerVideoGravityResizeAspectFill;
    
_previewlayer.frame = _qrView.layer.bounds;
```

绘制二维码页面:
```
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 191/255.0, 191/255.0, 191/255.0, 0.5);
    CGContextMoveToPoint(context, 0, 0);
    
    CGContextAddLineToPoint(context, rect.size.width, 0);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
    CGContextAddLineToPoint(context, 0, rect.size.height);
    
    CGContextFillPath(context);
    CGSize size = rect.size;
    CGContextClearRect(context, CGRectMake(QRRECT_ORIGN_X, QRRECT_ORIGN_Y, QRRECT_ORIGN_WIDTH,QRRECT_ORIGN_WIDTH));
    
    
    CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextSetLineWidth(context, 4);
    
    
    //    左上角
    CGContextMoveToPoint(context,rect.size.width/2 -100 , 110);
    CGContextAddLineToPoint(context, rect.size.width/2 -100, 100);
    CGContextAddLineToPoint(context, rect.size.width/2 -90, 100);
    
    //    右上角
    CGContextMoveToPoint(context,rect.size.width/2 +90 , 100);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 100);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 110);
    
    
    //    左下角
    CGContextMoveToPoint(context,rect.size.width/2 -100 , 290);
    CGContextAddLineToPoint(context, rect.size.width/2 -100, 300);
    CGContextAddLineToPoint(context, rect.size.width/2 -90, 300);
    
    //   右下角
    CGContextMoveToPoint(context,rect.size.width/2 +90 , 300);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 300);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 290);
    
    CGContextStrokePath(context);
}
```
扫描动画:
```
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationRepeatCount:1000];
    [UIView   setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationDuration:4.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [_middleBar setTransform:CGAffineTransformTranslate(_middleBar.transform ,0,200)];
    [UIView commitAnimations];
```
照明设置
```
//照明
- (void)torchSwitch:(UITapGestureRecognizer *)sender{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    _torchBar.selected = !_torchBar.selected;
    [_torchBar setTitle:@"关闭照明" forState:UIControlStateSelected];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];//开始配置
        if (_torchBar.selected) {
            [device setTorchMode:AVCaptureTorchModeOn];
        }else{
            [device setTorchMode:AVCaptureTorchModeOff];
        }
        [device unlockForConfiguration];//结束配置
    }
}
```
懒加载
```
//lazy load
- (QRView *)qrView{
    if (!_qrView) {
        _qrView = [[QRView alloc]initWithFrame:self.view.bounds];
        _qrView.backgroundColor = [UIColor clearColor];
    }
    return _qrView;
}
```

自定义相应方法
```
- (void)showTipsAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle  cAction:(SEL)cAction hasCancel:(BOOL)hasCancel{
    
    UIAlertController *alertVC =[UIAlertController alertControllerWithTitle:title message:subTitle preferredStyle:UIAlertControllerStyleAlert];
    __block  YHQVIewViewController *selfVC = self;
    __block SEL myAction = cAction;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
        if ( [selfVC respondsToSelector:cAction]) {
            [selfVC performSelector:cAction];
        }else{
            myAction = nil;
        }
        
    }];
    
    if (hasCancel) {
        UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault  handler:nil];
        [alertVC addAction:cancelBtn];
    }
    [alertVC addAction:okBtn];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

```

播放指示声音
```
 
     NSError *error;
    
    _audioPalyer = [[AVAudioPlayer alloc]initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"蜂鸣器声音" ofType:@"mp3"]] error:&error];
    
    BOOL ready = [_audioPalyer prepareToPlay];
    
    NSLog(@"audioPlayer ready = %@  error=%@",ready?@"YES":@"NO",error);
  if ([_audioPalyer prepareToPlay]) {
            [_audioPalyer play];
        }
```