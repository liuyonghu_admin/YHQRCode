//
//  AppDelegate.h
//  YHQRCode
//
//  Created by victorLiu on 2017/8/2.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

