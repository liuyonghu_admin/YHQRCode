//
//  UIViewController+YHAlertView.m
//  YHQRCode
//
//  Created by victorLiu on 2017/8/11.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "UIViewController+YHAlertView.h"

@implementation UIViewController (YHAlertView)
- (void)showTipsAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle  cAction:(SEL)cAction hasCancel:(BOOL)hasCancel{
    
    UIAlertController *alertVC =[UIAlertController alertControllerWithTitle:title message:subTitle preferredStyle:UIAlertControllerStyleAlert];
    __block  UIViewController *selfVC = self;
    __block SEL myAction = cAction;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
        if ( [selfVC respondsToSelector:cAction]) {
            [selfVC performSelector:cAction];
        }else{
            myAction = nil;
        }
        
    }];
    
    if (hasCancel) {
        UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault  handler:nil];
        [alertVC addAction:cancelBtn];
    }
    [alertVC addAction:okBtn];
    
    [self presentViewController:alertVC animated:YES completion:nil];


}
@end
