//
//  YHQVIewViewController.m
//  YHQRCode
//
//  Created by victorLiu on 2017/8/11.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//


#define sWidth  self.view.bounds.size.width
#define sHeight self.view.bounds.size.height


#import "YHQRView.h"
#import "YHQVIewViewController.h"
#import "UIViewController+YHAlertView.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
@interface YHQVIewViewController ()<AVCaptureMetadataOutputObjectsDelegate>
@property(nonatomic,strong)YHQRView *qrView;
@property(nonatomic,strong)AVCaptureSession *captureSession;
@property(nonatomic,strong)AVCaptureVideoPreviewLayer *previewlayer;
@property(nonatomic,strong)   CLLocationManager *locationManger ;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorView;
@property (strong,nonatomic)AVAudioPlayer *audioPalyer;

@end

@implementation YHQVIewViewController

-(void)loadView {
    [super loadView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSError *error;
    
    _audioPalyer = [[AVAudioPlayer alloc]initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"蜂鸣器声音" ofType:@"mp3"]] error:&error];
    
    BOOL ready = [_audioPalyer prepareToPlay];
    
    NSLog(@"audioPlayer ready = %@  error=%@",ready?@"YES":@"NO",error);
    
     [self setUpCaptureSession];
}


- (void)setUpCaptureSession{
    [self.view addSubview:self.qrView];
    
   
    AVCaptureDevice *device = [AVCaptureDevice  defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc]init];
    
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    //    设置扫描区域
    output.rectOfInterest = CGRectMake( 100/sHeight, (sWidth/2 -100)/sWidth,200/sHeight,200/sWidth);
    
    _captureSession = [[AVCaptureSession alloc]init];
    
    [_captureSession  setSessionPreset:AVCaptureSessionPresetHigh];
    
    [_captureSession addInput:input];
    
    [_captureSession addOutput:output];
    
    
    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    
    
    _previewlayer = [AVCaptureVideoPreviewLayer layerWithSession:_captureSession];
    
    //   显示方式  --  填充
    _previewlayer.videoGravity  = AVLayerVideoGravityResizeAspectFill;
    
    _previewlayer.frame = _qrView.layer.bounds;
    
    
    [self.view.layer insertSublayer:_previewlayer atIndex:0];
    
    
    //    开始扫描
    [_captureSession startRunning];
    
    //    开始扫描动画
    [_qrView startAnimations];
    
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    NSString *stringValue = @"";
    if ([metadataObjects count] >0)
    {
        //        播放指示声音
        if ([_audioPalyer prepareToPlay]) {
            [_audioPalyer play];
        }
        //        显示菊花效果
        [self showIndicatorView:YES];
        //        停止动画
        [_qrView.middleBar.layer removeAllAnimations];
        //停止扫描
        [_captureSession stopRunning];
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
        [self showTipsAlertWithTitle:@"提示" subTitle:stringValue cAction:@selector(backToForwardPage) hasCancel:NO];
    }
    
}

//lazy load
- (YHQRView *)qrView{
    if (!_qrView) {
        _qrView = [[YHQRView alloc]initWithFrame:self.view.bounds];
                _qrView.backgroundColor = [UIColor clearColor];
//        _qrView.backgroundColor = [UIColor redColor];
    }
    return _qrView;
}


- (void)showIndicatorView:(BOOL)YesOrNO{
    if (YesOrNO) {
        _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_indicatorView setCenter:CGPointMake(sWidth/2, sHeight/2)];
        
        [self.view addSubview:_indicatorView];
        
        [_indicatorView startAnimating];
    }else{
        if (_indicatorView != nil) {
            [_indicatorView stopAnimating];
            [_indicatorView removeFromSuperview];
            _indicatorView = nil;
            [_captureSession startRunning];
            [_qrView startAnimations];
        }
       
    }
   
    
}

- (void)backToForwardPage{
    [self showIndicatorView:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
