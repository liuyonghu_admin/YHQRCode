//
//  ViewController.m
//  YHQRCode
//
//  Created by victorLiu on 2017/8/2.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//



#import "ViewController.h"
#import "YHQVIewViewController.h"
#import "UIViewController+YHAlertView.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController ()
@property(nonatomic,strong)   CLLocationManager *locationManger ;
@property (weak, nonatomic) IBOutlet UIButton *requestForAuthority;
@property (weak, nonatomic) IBOutlet UIButton *showQRPage;
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}




- (IBAction)accessToRight:(id)sender {
    _locationManger =  [[CLLocationManager alloc]init];
    [_locationManger requestWhenInUseAuthorization];
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus status = [AVCaptureDevice  authorizationStatusForMediaType:mediaType];
    if (status == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            
        }];
    }
    
    else if (status == AVAuthorizationStatusDenied|| status == AVAuthorizationStatusRestricted){
        [self showTipsAlertWithTitle:@"提示" subTitle:@"请前往->设置->设置权限" cAction:@selector(goToSettingPage) hasCancel:YES];
    }
    
    else if (status == AVAuthorizationStatusAuthorized) {
        [self showTipsAlertWithTitle:@"提示" subTitle:@"您已经设置权限" cAction:nil hasCancel:NO];
    }
    
}
- (void)goToSettingPage{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{UIApplicationOpenURLOptionsSourceApplicationKey:@"com.payease.UnitTest.YHQRCode"} completionHandler:^(BOOL success) {
        
    }];
}

- (IBAction)showScanPage:(id)sender {
    
    NSString *mediaType = AVMediaTypeVideo;
    
    AVAuthorizationStatus  status = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    
    switch (status) {
        case AVAuthorizationStatusNotDetermined:{
            [self goToSettingPage];
        }
            break;
        case AVAuthorizationStatusAuthorized:{
            
            YHQVIewViewController *yhQRViewVC = [[YHQVIewViewController alloc]init];
            [self.navigationController pushViewController:yhQRViewVC animated:YES];
            
        }
            break;
        default:{
            [self showTipsAlertWithTitle:@"提示" subTitle:@"请前往->设置->设置权限" cAction:@selector(goToSettingPage) hasCancel:YES];
        }
            break;
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
