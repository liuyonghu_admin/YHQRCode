//
//  YHQRView.m
//  YHQRCode
//
//  Created by victorLiu on 2017/8/11.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//


#define QRRECT_ORIGN_X  size.width/2 -100
#define QRRECT_ORIGN_Y  100
#define QRRECT_ORIGN_WIDTH  200


#import <AVFoundation/AVFoundation.h>
#import "YHQRView.h"

@interface YHQRView ()
@property(nonatomic,strong)UIButton *torchBar;
@end


@implementation YHQRView

- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 191/255.0, 191/255.0, 191/255.0, 0.5);
    CGContextMoveToPoint(context, 0, 0);
    
    CGContextAddLineToPoint(context, rect.size.width, 0);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
    CGContextAddLineToPoint(context, 0, rect.size.height);
    
    CGContextFillPath(context);
    CGSize size = rect.size;
    CGContextClearRect(context, CGRectMake(QRRECT_ORIGN_X, QRRECT_ORIGN_Y, QRRECT_ORIGN_WIDTH,QRRECT_ORIGN_WIDTH));
    
    
    CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextSetLineWidth(context, 4);
    
    
    //    左上角
    CGContextMoveToPoint(context,rect.size.width/2 -100 , 110);
    CGContextAddLineToPoint(context, rect.size.width/2 -100, 100);
    CGContextAddLineToPoint(context, rect.size.width/2 -90, 100);
    
    //    右上角
    CGContextMoveToPoint(context,rect.size.width/2 +90 , 100);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 100);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 110);
    
    
    //    左下角
    CGContextMoveToPoint(context,rect.size.width/2 -100 , 290);
    CGContextAddLineToPoint(context, rect.size.width/2 -100, 300);
    CGContextAddLineToPoint(context, rect.size.width/2 -90, 300);
    
    //   右下角
    CGContextMoveToPoint(context,rect.size.width/2 +90 , 300);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 300);
    CGContextAddLineToPoint(context, rect.size.width/2 +100, 290);
    
    CGContextStrokePath(context);
}


-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        _middleBar = [[UIView alloc]init];
        CGSize size = frame.size;
        [_middleBar setFrame:CGRectMake(QRRECT_ORIGN_X, QRRECT_ORIGN_Y, QRRECT_ORIGN_WIDTH,2)];
        _middleBar.layer.masksToBounds = YES;
        _middleBar.layer.cornerRadius = 10;
        _middleBar.layer.opacity = 0.9;
        [_middleBar setBackgroundColor:[UIColor greenColor]];
        [self addSubview:_middleBar];
        
        
        _torchBar = [UIButton buttonWithType:UIButtonTypeSystem];
        [_torchBar setTitle:@"打开照明" forState:UIControlStateNormal];
        [_torchBar setFrame:CGRectMake(QRRECT_ORIGN_X, QRRECT_ORIGN_Y +300, QRRECT_ORIGN_WIDTH,30)];
        UIBlurEffect *bluerEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:bluerEffect];
        effectView.userInteractionEnabled =YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(torchSwitch:)];
        [effectView addGestureRecognizer:tapGesture];
        effectView.frame = _torchBar.bounds;
        _torchBar.selected = NO;
        [_torchBar addSubview:effectView];
        [self addSubview:_torchBar];
    }
    
    return self;
}

//扫描动画
- (void)startAnimations{
//    CGAffineTransformMake(<#CGFloat a#>, <#CGFloat b#>, <#CGFloat c#>, <#CGFloat d#>, <#CGFloat tx#>, <#CGFloat ty#>)
     [_middleBar setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationRepeatCount:1000];
    [UIView   setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationDuration:4.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    NSLog(@"transfrom = %@",NSStringFromCGAffineTransform(_middleBar.transform));
    [_middleBar setTransform:CGAffineTransformTranslate(_middleBar.transform ,0,200)];
    [UIView commitAnimations];
}

//照明
- (void)torchSwitch:(UITapGestureRecognizer *)sender{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    _torchBar.selected = !_torchBar.selected;
    [_torchBar setTitle:@"关闭照明" forState:UIControlStateSelected];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];//开始配置
        if (_torchBar.selected) {
            [device setTorchMode:AVCaptureTorchModeOn];
        }else{
            [device setTorchMode:AVCaptureTorchModeOff];
        }
        [device unlockForConfiguration];//结束配置
    }
}


@end
