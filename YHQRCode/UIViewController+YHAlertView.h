//
//  UIViewController+YHAlertView.h
//  YHQRCode
//
//  Created by victorLiu on 2017/8/11.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (YHAlertView)
- (void)showTipsAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle  cAction:(SEL)cAction hasCancel:(BOOL)hasCancel;
@end
