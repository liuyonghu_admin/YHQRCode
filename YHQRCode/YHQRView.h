//
//  YHQRView.h
//  YHQRCode
//
//  Created by victorLiu on 2017/8/11.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YHQRView : UIView
@property(nonatomic,strong)UIView *middleBar;

- (void)startAnimations;
@end

